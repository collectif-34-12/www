import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'

import Layout from '../components/Layout'
import Testimonials from '../components/Testimonials'
import BlogRoll from '../components/BlogRoll'
import Content, { HTMLContent } from '../components/Content'

export const IndexPageTemplate = ({
  image,
  title,
  heading,
  subheading,
  mainpitch,
  content,
  contentComponent,
  testimonials,
}) =>  {
  const PageContent = contentComponent || Content
  return (
    <div>
      <div
        className="full-width-image margin-top-0"
        style={{
          backgroundImage: `url(${
            !!image.childImageSharp ? image.childImageSharp.fluid.src :
            image
          })`,
          backgroundPosition: `top left`,
          backgroundAttachment: `fixed`,
        }}
      >
        <div
          style={{
            display: 'flex',
          height: '150px',
          lineHeight: '1',
          justifyContent: 'space-around',
          alignItems: 'left',
          flexDirection: 'column',
        }}
      >
        <h1
          className="has-text-weight-bold is-size-3-mobile is-size-2-tablet is-size-1-widescreen"
          style={{
            boxShadow:
              '#627e00 0.5rem 0px 0px, #627e00 -0.5rem 0px 0px',
            backgroundColor: '#627e00',
            color: 'white',
            lineHeight: '1',
            padding: '0.25em',
          }}
        >
          {title}
        </h1>
        <h3
          className="has-text-weight-bold is-size-5-mobile is-size-5-tablet is-size-4-widescreen"
          style={{
            boxShadow:
              '#e83327ff 0.5rem 0px 0px, #e83327ff -0.5rem 0px 0px',
            backgroundColor: '#e83327ff',
            color: 'white',
            lineHeight: '1',
            padding: '0.25em',
          }}
        >
          {subheading}
        </h3>
      </div>
    </div>
    <section className="section section--gradient">
      <div className="container">
        <div className="section">
          <div className="columns">
            <div className="column is-10 is-offset-1">
              <div className="content">
                <div className="content">
                  <div className="tile">
                    <h1 className="title">{mainpitch.title}</h1>
                  </div>
                  <div className="tile">
                    <h3 className="subtitle">{mainpitch.description}</h3>
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-12 has-text-centered">
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      className="red-btn"
                      title="NON aux centrales éoliennes dans l'Escandorgue"
                      href="https://www.change.org/p/non-aux-centrales-%C3%A9oliennes-dans-les-monts-d-arr%C3%A9e">
                      Signer la pétition
                    </a>
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-12">
                    <h3 className="has-text-weight-semibold is-size-2">
                      {heading}
                    </h3>
                    <PageContent content={content} />
                    <br />
                    <div className="has-text-centered">
                      <a
                        target="_blank"
                        rel="noopener noreferrer"
                        className="red-btn"
                        title="NON aux centrales éoliennes dans l'Escandorgue"
                        href="https://www.change.org/p/non-aux-centrales-%C3%A9oliennes-dans-les-monts-d-arr%C3%A9e">
                        Signer la pétition
                      </a>
                    </div>

                    <h3 className="has-text-weight-semibold is-size-2">
                      Écoutons les riverains des centrales éoliennes
                    </h3>
                    <Testimonials testimonials={testimonials} />
                  </div>
                </div>

                <div className="columns">
                  <div className="column is-12 has-text-centered">
                    <Link className="btn" to="/eoliennes">
                      En savoir plus sur les éoliennes industrielles
                    </Link>
                  </div>
                </div>
                <div className="column is-12">
                  <h3 className="has-text-weight-semibold is-size-2">
                    Dernières nouvelles
                  </h3>
                  <BlogRoll />
                  <div className="column is-12 has-text-centered">
                    <Link className="btn" to="/blog">
                      Toutes les nouvelles
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>)
}

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
  mainpitch: PropTypes.object,
  content: PropTypes.node.isRequired,
  contentComponent: PropTypes.func,
  testimonials: PropTypes.array,
}

const IndexPage = ({ data }) => {
  const { markdownRemark: page } = data

  return (
    <Layout>
      <IndexPageTemplate
        image={page.frontmatter.image}
        title={page.frontmatter.title}
        heading={page.frontmatter.heading}
        subheading={page.frontmatter.subheading}
        mainpitch={page.frontmatter.mainpitch}
        content={page.html}
        contentComponent={HTMLContent}
        testimonials={page.frontmatter.testimonials}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      html
      frontmatter {
        title
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 80) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        heading
        subheading
        mainpitch {
          title
          description
        }
        testimonials {
          title
          video
          author
          quote
        }
      }
    }
  }
`
