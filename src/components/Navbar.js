import React from 'react'
import { Link } from 'gatsby'
import gitlab from '../img/gitlab-icon.svg'
import facebook from '../img/social/facebook.svg'
import twitter from '../img/social/twitter.svg'
import logo from '../img/logo.png'

const Navbar = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      navBarActiveClass: '',
    }
  }

  toggleHamburger = () => {
    // toggle the active boolean in the state
    this.setState(
      {
        active: !this.state.active,
      },
      // after state has been updated,
      () => {
        // set the class in state for the navbar accordingly
        this.state.active
          ? this.setState({
              navBarActiveClass: 'is-active',
            })
          : this.setState({
              navBarActiveClass: '',
            })
      }
    )
  }

  render() {
    return (
      <nav
        className="navbar is-transparent"
        role="navigation"
        aria-label="main-navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            <Link to="/" className="navbar-item" title="Collectif 34-12">
              <img src={logo} alt="Collectif 34-12" style={{ width: '2.5rem' }} />
            </Link>
            {/* Hamburger menu */}
            <div
              className={`navbar-burger burger ${this.state.navBarActiveClass}`}
              data-target="navMenu"
              onClick={() => this.toggleHamburger()}
            >
              <span />
              <span />
              <span />
            </div>
          </div>
          <div
            id="navMenu"
            className={`navbar-menu ${this.state.navBarActiveClass}`}
          >
            <div className="navbar-start has-text-centered">
              <Link className="navbar-item" to="/collectif-34-12">
                Le Collectif
              </Link>
              <Link className="navbar-item" to="/eoliennes">
                Les éoliennes
              </Link>
              <Link className="navbar-item" to="/blog">
                Nouvelles
              </Link>
              <Link className="navbar-item" to="/contact">
                Contact
              </Link>
              <a
                target="_blank"
                rel="noopener noreferrer"
                className="navbar-item"
                title="NON aux centrales éoliennes dans l'Escandorgue"
                href="https://www.change.org/p/">
                Signer la pétition
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                className="navbar-item"
                title="Faire un don via HelloAsso"
                href="https://www.donnerenligne.fr//faire-un-don">
                Faire un don
              </a>
            </div>
            <div className="navbar-end has-text-centered">
             <a
                className="navbar-item"
                href="https://www.facebook.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={facebook} alt="Facebook" />
                </span>
              </a>
              <a
                className="navbar-item"
                href="https://twitter.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={twitter} alt="Twitter" />
                </span>

              </a>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
