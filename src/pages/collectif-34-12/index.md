---
templateKey: 'about-page'
path: /collectif-34-12
title: Le Collectif 34-12
---
### Protéger le cadre de vie, la faune et de la flore de l'Escandorgue :

c'est la mission que nous nous sommes fixée. Plus formellement, voici l'objet de l'association tel que défini dans <a href="https://mypads.framapad.org/p/status-v2-j73q57lv" target="_blank" rel="noopener noreferrer">nos statuts</a> :

  - Lutter contre tous les projets industriels, notamment les usines d’aérogénérateurs, qui portent atteinte aux humains, à la biodiversité, aux paysages et aux écosystèmes de l'Escandorgue ;
  - Défendre l’identité culturelle des paysages, du patrimoine, de la ruralité, leur équilibre, leur salubrité ainsi que leurs intérêts économiques, touristiques, historiques et sociaux.

### Pour et par les habitants de l'Escandorgue

<a href="https://reporterre.net/Pour-sauver-la-planete-l-industrie-tue-les-campagnes" target="_blank" rel="noopener noreferrer">Pour « sauver la planète », l’industrie tue les campagnes</a>. Nous, habitants de l'Escandorgue, refusons cette "fatalité". Nous pensons que la transition énergétique doit à la fois être :
 - raisonnée (s'attaquer aux vrais problèmes : consommation d'énergie fossile, épuisement des ressources naturelles, destructions des écosystèmes) et
 - <a href="https://fr.wikipedia.org/wiki/Technologie_interm%C3%A9diaire" target="_blank" rel="noopener noreferrer">appropriée</a> par la population.

Les centrales éoliennes industrielles sont donc de fausses solutions, en particulier dans un espace naturel protégé tel que l'Escandorgue.

Nous voulons donc informer tous les habitants et créer un consensus de la population qui puisse servir de base à l'action de nos instances représentatives (commune, intercommunalité).

### Une organisation collégiale

Pas de titre de président, nous avons opté pour un mode de gouvernance horizontal : l'administration est collégiale (actuellement six "administrateurs").

Le collectif est ouverte à tous ; nous vous invitons à prendre contact avec nous via [le formulaire](/contact) ou directement par email : contact@collectif-34-12.org.

### Soutenez l'association

Nous vous invitons à venir nous rencontrer, à rejoindre l'association, à participer à ses décisions et à ses actions.

Vous pouvez également soutenir l'association financièrement :
