---
templateKey: eoliennes-page
title: NON AUX CENTRALES ÉOLIENNES DANS l'ESCANDORGUE
image: /img/eoliennes.png
heading: Protégeons les derniers refuges de la faune et de la flore !
testimonials:
  - author: 'Jean-Dominique Barraud, maire de Lavoine'
    quote: >-
      Les gens y allaient de bon cœur pour toucher le pactole, le super
      jackpot... Mais le jackpot maintenant, c'est les conséquences.
    title: >-
      Témoignages d'habitants qui subissent le voisinage des éoliennes
      industrielles dans l'Allier.
    video: 'https://www.youtube-nocookie.com/embed/Sa3dRnWBRps'
  - author: 'Yann Joly, éleveur'
    quote: >-
      en 2011, baisse de presque 2000 litres de production de lait par vache.
      [...] En 2015, je n'ai pas eu d'autre solution que de licencier mon
      salarié et d'arrêter l'activité laitière.
    title: L'impact des éoliennes sur le bétail
    video: 'https://www.youtube-nocookie.com/embed/Ur3-nTlo4AE'
  - author: 'Thomas Hennequin, conseiller municipal à Lislet (02)'
    quote: >-
      Je pense que les deux avis sont présents dans le conseil municipal
      actuellement, mais le gros problème c'est que, pour certaines personnes,
      c'est le versant financier qui a pris le dessus sur le versant écologique.
    title: Un référendum local dit non aux éoliennes
    video: 'https://www.youtube-nocookie.com/embed/TVIIPNszyj0'
  - author: Jean-Marc Jancovici (Physicien)
    quote: >-
      "si vous voulez augmenter la puissance garantie vous serez obligé
      d'augmenter la puissance pilotable, donc même si vous voulez mettre des
      éoliennes vous serez obligés d'ajouter autre chose avec"
    title: >-
      commission d'enquête parlementaire sur les énergies renouvelables (Impact
      économique, industriel et environnemental des énergies renouvelables)
    video: 'https://www.youtube-nocookie.com/embed/Hr9VlAM71O0'
  - author: >-
      Marjolaine Meynier Millefert - Députée, Rapporteure de la Commission
      d’enquête parlementair sur les énergies renouvelables et la transition
      énergétique
    quote: >-
      80 % des gens qui vous disent que le développement des ENR électriques en
      France soutient la décarbonation **  et finalement la transition
      écologique en France je pense que ce n’est  pas bon non plus parce que le
      jour où les gens vont vraiment comprendre que cette transition énergétique
      ne sert pas la transition écologique vous aurait une réaction de rejet de
      ces politiques en disant vous avez menti ....
    title: Le déploiement des éoliennes n'est PAS la transition écologique
    video: 'https://www.youtube-nocookie.com/embed/3a0iH11CTS0'
  - author: 'Jean-Marc Jancovici - ingénieur, spécialiste de l''énergie et du climat.'
    quote: >-
      Tous en coeur, les gaziers disaient "L'éolien, c'est géniral ! L'éolien
      c'est génial !"
    title: Le problème de l'intermittence des renouvelables
    video: 'https://www.youtube-nocookie.com/embed/q3QHOp9BpLo'
---
* Notre santé en danger
* Perte de valeur de notre patrimoine et d’attractivité de notre territoire
* Paysages défigurés
* Dévalorisation des biens immobiliers
* Perte de biodiversité et fuite du gibier
* Augmentation de nos factures d’énergie sans diminution des émissions de CO2
* Finies les nuits étoilées !

L’Escandorgue sont actuellement la proie de promoteurs éoliens, et la commune de Lunas est une de leurs premières cibles.

La Mairie a initié un projet éolien sur la commune de Lunas, sans consultation de ses habitants. Une association de « citoyens-actionnaires » a même été créée à l’initiative de la Mairie, afin d’entrer dans une « société de projet » tripartite (promoteur éolien, commune et cette association). Il s’agit en réalité d’un simulacre de démocratie.

Les éoliennes seraient implantées entre le Bourg, Quinoualc’h, Cozcastel, Keraden, Kernevez et Tredudon-le-Moine. Un tel projet paraît insensé aux yeux des habitants qui aiment l’Escandorgue pour la beauté de ses paysages et pour la qualité de vie qu’on peut y trouver.

**Seul un NON ferme de la population pourra empêcher ce projet !**

# Documents sur le projet éolien de Lunas

La mairie a très peu communiqué et n'a rien publié sur le projet. Des membres de notre association ont tout de même été se renseigner auprès de la mairie et de personnes ayant assisté aux réunions préparatoires (non publiques). Voici les documents que nous avons obtenus :

* [Structuration juridique : Rencontre des partenaires potentiels](/img/20190625_synthese_rencontre_partenaires.pdf)
* [Restitution prédiagnostic et construction d’un projet citoyen](/img/20190708_synthese_echange_citoyens_complete.pdf)
* [Appel à projet (recherche d'un développeur éolien)](/img/appel%20à%20projet.pdf)

![Emplacements possibles des éoliennes du projet](/img/carte-projet-éolien-Lunas.jpeg "Emplacements possibles des éoliennes du projet")

Plusieurs de ces documents mentionnent l’existence d'une pre-étude technique détaillant en particulier les impacts sur la faune, la flore et les paysages. Ce document ne nous a pas été communiqué, sous prétexte que nous pourrions faire cette étude nous-mêmes, les données utilisées étant publiques.

# Se documenter sur l'éolien industriel

Des liens et sources seront ajoutés au fur et à mesure de nos recherches. Cependant nous souhaitons mettre en avant deux guides élaborés par l'Association Collectif Bourgogne Franche-Comté, qui, même si nous ne sommes pas tous d'accord avec l'ensemble des arguments, a fait un travail de synthèse remarquable :

* **[Guide de l'éolien à l'attention de l'élu et du citoyen](/img/Guide%20de%20l'éolien%20et%20de%20l'élu.pdf)**
* **[Prise en charge d'un projet éolien](/img/Prise%20en%20charge%20d'un%20projet%20éolien%20v1.1.pdf)**

Ce dernier  guide est très important si vous souhaitez créer sur votre commune une association d'opposition aux projets néfastes, tels que les usines d'aérogénérateurs.

Consultez également [leur site](http://acbfc.blogspot.com/p/documents-divers.html) pour plus d'informations.

En ce qui nous concerne, voici une synthèse des raisons qui pousse notre association à s'opposer aux usines d'aérogénérateurs, à Lunas et dans l'Escandorgue :

## Des profits privés aux dépens du climat !

Plusieurs milliards d’euros par an sont prélevés sur nos factures d’électricité (CSPE) et de carburants pour subventionner des promoteurs éoliens privés. En France, le déploiement de l’éolien ne permet pas de réduire efficacement les émissions de gaz à effet de serre ; au contraire il est souvent associé aux centrales à gaz puisqu’il faut de toutes façons une source d’électricité rapidement pilotable quand il n’y a pas de vent ! Cet argent donné aux promoteurs pourrait servir à financer des mesures d’économie d’énergie (isolation des bâtiments…), les énergies renouvelables thermiques (pompes à chaleur…), les transports en commun, etc. bien plus efficaces pour le climat et pour pouvoir enfin fermer des centrales ! En plus, l’éolien industriel cause d’autres [problèmes écologiques](https://reporterre.net/L-eolien-signe-la-fracture-entre-deux-visions-de-l-ecologie) : pales non recyclables, des kilomètres de lignes électriques à tirer et des transformateurs électriques à ajouter, consommation de grandes quantités de matériaux…

## Des paysages ruinés !

L’Escandorgue : ils regorgent d’espaces naturels remarquables d’une exceptionnelle diversité, qui sont au cœur de l’attractivité du territoire. Ce patrimoine naturel unique apparaît comme un élément-clé de l’identité régionale et contribue largement à la qualité de la vie locale et au tourisme. Il doit être préservé des projets industriels qui portent atteinte aux humains, à la biodiversité, aux paysages et aux écosystèmes de l'Escandorgue. D’autres projets seraient en cours à La Feuillée, Scrignac, Brennilis, Le Cloître-Saint-Thégonnec… L’expérience montre que là où des éoliennes sont implantées une première fois, d’autres suivent jusqu’à saturation des paysages. De plus, là où il y a des éoliennes, les maisons et gîtes ruraux perdent entre 20 et 45% de leur valeur et l’économie locale peut être impactée négativement, notamment le tourisme.

## La biodiversité massacrée !

Les éoliennes industrielles créent un vide écologique autour d’elles : fuite du gibier, [hécatombe](https://sciencepost.fr/les-eoliennes-sont-a-lorigine-de-la-disparition-dau-moins-250-000-chauves-souris-par-an-en-france/) de [chauve-souris](http://www.vigienature.fr/fr/actualites/chauves-souris-eoliennes-liaisons-dangereuses-3303) et d’[oiseaux de proie protégés](https://www.lpo.fr/communiques-de-presse/eoliennes-et-biodiversite-synthese-des-connaissances-sur-les-impacts-et-les-moyens-de-les-attenuer-dp1), destruction de leurs habitats, artificialisation des sols… De nuit, les lumières aveuglantes des éoliennes perturbent également les oiseaux migrateurs et stressent les animaux qui ne peuvent plus se reposer sereinement.

## Notre santé et les élevages en danger !

L’État ne mène aucune étude approfondie sur les impacts des éoliennes sur la santé des humains et des animaux. Or certains éleveurs subissent des pertes importantes, en qualité et quantité pour le lait, et une mortalité anormale dans leurs troupeaux, par exemple à Nozay (44), qui pourrait s'expliquer par **[l'action conjointe du champs électro-magnétique des éoliennes et d'une contamination aux terres rares](https://actu.fr/pays-de-la-loire/nozay_44113/vaches-mortes-eleveurs-malades-nozay-une-contamination-metaux-revelee_31788801.html)**. Des études ont aussi montré des taux de cortisol (marqueur du stress) plus élevés dans des élevages de porcs proches d’éoliennes. Dans d'autres pays d’Europe la distance minimale aux habitations est bien plus élevée qu’en France où elle est seulement de 500 mètres pour des éoliennes qui peuvent désormais atteindre plus de 200 mètres de haut ! L’impact sonore sur les riverains est évalué à 35 dBA (déjà désagréable, le bruit des éoliennes étant quasi permanent) mais est en fait bien plus important pour certaines fréquences d’infrasons et il peut y avoir des résonances aux effets multiples (véranda qui vibre, échos dus à la topographie des lieux…). [Les personnes](https://www.youtube-nocookie.com/embed/J4DB7guRGyo) vivant à proximité sont dérangées par les flashs lumineux rouges la nuit et par des effets stroboscopiques le jour…

## L’arnaque des baux emphytéotiques…

Les industriels éoliens préfèrent toujours louer les terres, alors que cela leur coûterait beaucoup moins cher de les acheter. Pourquoi ? Lorsqu’une éolienne est obsolète et que l’exploitant (souvent une société-écran) dépose le bilan, c’est le propriétaire qui est responsable de ce qui est sur son terrain. Dans ce cas, il ne peut compter que sur les 50 000 euros qui sont provisionnés (obligatoirement) pour le démantèlement d’une éolienne, alors qu’en réalité cela coûte plus de 150 000 euros ! Et en dernier recours c’est le contribuable qui paiera, donc nous tous ! Et même si l’exploitant effectue le démantèlement, il n’a aucune obligation d’enlever les fondations en béton armé (jusqu’à 1500 tonnes par pied d’éolienne), qui, en se dégradant, pollueront l’environnement !
