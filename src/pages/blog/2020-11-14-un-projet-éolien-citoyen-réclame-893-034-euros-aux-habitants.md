---
templateKey: blog-post
title: Un projet éolien "citoyen" réclame 893 034 euros aux habitants
date: 2020-11-14T17:26:07.464Z
description: >-
  Un projet "citoyen" et "participatif", qu'ils disaient...


  Une mise en garde de la Société pour la Protection des Paysages et de
  l’Esthétique de la France (SPPEF). 

  À lire absolument pour bien comprendre la tromperie, 
featuredpost: true
featuredimage: /img/total-quadran-eolien.jpg
tags:
  - eolien total citoyen
---
## [Éolien en forêt de Taillard (PNR du Pilat) : l’intimidation inédite d’un industriel du vent](https://www.sppef.fr/2020/11/12/eolien-en-foret-de-taillard-pnr-du-pilat-lintimidation-inedite-dun-industriel-du-vent/)

Mairies et communautés de communes : ne tombez pas dans le panneau "projet citoyen", vous ne ferez jamais le poids face aux intérêts financiers en jeu !

Ne leur apportez pas votre caution !

Restez libres, pour pouvoir défendre les intérêts des citoyens face aux intérêts financiers des industriels et des actionnaires privés !

N'oubliez pas de signer notre pétition : https://www.change.org/p/non-aux-centrales-%C3%A9oliennes-dans-les-monts-d-arr%C3%A9e
